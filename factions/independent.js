const _ = require('lodash');

module.exports = {
    id: 'independent',
    name: 'Torn Soul',
    description: `Hope? Despair? You are torn between both. Your mind is filled with confusion and madness. As you can not side with either of them, you win only when you are the last one standing!`,
    isVictory: function(p) {
        var livePlayers = _.filter(p.game.players, 'alive');
        // only self is alive
        return livePlayers.length === 1 && livePlayers[0].id === p.player.id;
    },
};
