const _ = require('lodash');

module.exports = {
    id: 'town',
    name: 'Hope',
    description: `You win when all the desperate and torn souls (Mafia, Serial Killers, etc.) are executed. However, you don't know exactly who else might carry the gloom of despair...`,
    isVictory: function(p) {
        // only townies left alive
        var livePlayers = _.filter(p.game.players, 'alive');
        var townieCount = _.filter(livePlayers, {faction: this.id}).length;
        return townieCount === livePlayers.length;
    },
};
