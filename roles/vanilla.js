const ext = require('../lib/ext.js');

module.exports = ext(require('./tmpls/noAction.js'), {
    id: 'vanilla',
    name: 'Ultimate Vanilla',
    description: `Wow! You are an ultimate! You probably have some super special ability, but it has no use for this game. Like... uh.. fishing? Therefore, you have no special abilities and you actually need to win by talking. How boring!`,
});
