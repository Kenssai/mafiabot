const ext = require('../lib/ext.js');

module.exports = ext(require('./tmpls/noAction.js'), {
    id: 'godfather',
    name: 'Ultimate Godfather',
    description: `You have no active abilities, but you get scanned as innocent by cops. This will cause endless despair!`,
}, require('./mods/inno.js').mod);
