const ext = require('../lib/ext.js');

module.exports = ext(require('./tmpls/noAction.js'), {
    id: 'miller',
    name: 'Ultimate Miller',
    description: `You have no active abilities, but you get scanned as scum by cops. How does that feel to be framed as scum by everyone?`,
}, require('./mods/miller.js').mod);
